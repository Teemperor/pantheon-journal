
namespace Journal {
    public class JournalToolbar : Gtk.HeaderBar {

        Gtk.ToolButton load_button;
        Gtk.ToolButton save_button;

        public JournalToolbar () {
            title = "Journal";
            show_close_button = true;

            load_button = create_button ("document-open", () => {

            });
            pack_start (load_button);


            save_button = create_button ("document-save", () => {

            });
            pack_start (save_button);


            Gtk.ColorButton button = new Gtk.ColorButton ();
            // Sets the title for the color selection dialog:
            button.set_title (("Select the pen-color"));
            // Catch color-changes:
            button.color_set.connect (() => {
                GlobalPaintSettings.instance.color = new PaintColor.from_rgba ((button as Gtk.ColorChooser).rgba);
            });
            pack_start (button);


            //eraser button
            {
                Gtk.Image img = new Gtk.Image.from_icon_name ("edit-clear", Gtk.IconSize.SMALL_TOOLBAR);
                Gtk.ToggleToolButton erase_button = new Gtk.ToggleToolButton ();
                erase_button.set_icon_widget (img);
                erase_button.toggled.connect (() => {
                    message (erase_button.active.to_string ());
                    GlobalPaintSettings.instance.eraser_mode = erase_button.active;
                });
                add (erase_button);
            }
        }

        private delegate void ButtonAction ();

        private Gtk.ToolButton create_button (string icon_name, ButtonAction action) {
            Gtk.Image img = new Gtk.Image.from_icon_name (icon_name, Gtk.IconSize.SMALL_TOOLBAR);
            Gtk.ToolButton result = new Gtk.ToolButton (img, null);
            result.clicked.connect (() => {
                action ();
            });
            return result;
        }
    }
}