

namespace Journal {

    public class EntryWidget : Gtk.Grid {
        Gtk.Image image;

        Gtk.EventBox eventbox = new Gtk.EventBox ();

        PaintablePixbuf buf;

        double last_x = -1;
        double last_y = -1;
        uint last_button = -1;

        EntryList list;

        public EntryWidget (EntryList? list = null) {
            this.list = list;

            buf = new PaintablePixbuf (700, 1000);
            buf.color = GlobalPaintSettings.instance.color;
            buf.eraser_mode = GlobalPaintSettings.instance.eraser_mode;

            image = new Gtk.Image ();
            image.set_from_pixbuf (buf.get_pixbuf ());
            eventbox.add (image);

            attach (eventbox, 0, 0, 1, 1);
            eventbox.add_events (Gdk.EventMask.SMOOTH_SCROLL_MASK);
            eventbox.add_events (Gdk.EventMask.BUTTON1_MOTION_MASK);

            var grid_pixbuf = new GridPixbuf (700, 1000);
            var background_image = new Gtk.Image ();
            background_image.set_from_pixbuf (grid_pixbuf.get_pixbuf ());

            attach (background_image, 0, 0, 1, 1);

            margin_bottom = 10;

            GlobalPaintSettings.instance.color_changed.connect ((color) => {
                buf.color = color;
            });

            GlobalPaintSettings.instance.eraser_mode_changed.connect ((mode) => {
                buf.eraser_mode = mode;
            });

            button_press_event.connect ((e) => {
                if (e.button == 1) {
                    last_x = e.x;
                    last_y = e.y;
                }

                last_button = e.button;
                return true;
            });

            motion_notify_event.connect ((e) => {
                int tx = (int) e.x;
                int ty = (int) e.y;

                if (last_x != -1 && last_y != -1) {
                    if (last_button == 1) {
                        buf.draw_line ((int) last_x, (int) last_y, tx, ty);
                    }
                }
                last_x = e.x;
                last_y = e.y;
                image.set_from_pixbuf (buf.get_pixbuf ());

                return true;
            });
        }
    }
}