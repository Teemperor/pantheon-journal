namespace Journal {
    public class PaintColor {
        public uint8 r;
        public uint8 g;
        public uint8 b;
        public uint8 a;

        public PaintColor (uint8 r = 0, uint8 g = 0, uint8 b = 0, uint8 a = 0) {
            init (r, g, b, a);
        }

        private void init (uint8 r = 0, uint8 g = 0, uint8 b = 0, uint8 a = 0) {
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
        }

        public PaintColor.from_string (string s) {
            switch (s) {
                case "black":
                    init (0, 0, 0, 255);
                break;

                case "white":
                    init (255, 255, 255, 255);
                break;
            }
        }

        public PaintColor.from_rgba (Gdk.RGBA rgba) {
            r = (uint8) (255 * rgba.red);
            g = (uint8) (255 * rgba.green);
            b = (uint8) (255 * rgba.blue);
            a = 255;
        }
    }
}