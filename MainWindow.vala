

namespace Journal {
    public class MainWindow : Gtk.Window {

        public JournalToolbar toolbar { get; private set; }

        EntryList entry_list;

        public MainWindow () {
            title = "Journal";
            window_position = Gtk.WindowPosition.CENTER;
            set_default_size (350, 70);

            entry_list = new EntryList ();
            add (entry_list);

            toolbar = new JournalToolbar ();
            set_titlebar (toolbar);

            destroy.connect (Gtk.main_quit);
        }
    }
}