
namespace Journal {

    int main (string[] args) {
        Gtk.init (ref args);

        var window = new MainWindow ();
        window.set_default_size (800, 600);
        window.show_all ();

        Gtk.main ();
        return 0;
    }

}