

namespace Journal {

    public class GlobalPaintSettings {

        PaintColor _color = new PaintColor (0, 0, 0, 255);
        public PaintColor color {
            get {
                return _color;
            }
            set {
                color_changed (value);
                _color = value;
            }
        }

        bool _eraser_mode = false;
        public bool eraser_mode {
            get {
                return _eraser_mode;
            }
            set {
                eraser_mode_changed (value);
                _eraser_mode = value;
            }
        }

        public signal void color_changed (PaintColor color);
        public signal void eraser_mode_changed (bool new_mode);

        private GlobalPaintSettings () {
        }

        //singleton pattern
        static GlobalPaintSettings _instance = null;
        public static GlobalPaintSettings instance {
            get {
                if (_instance == null) {
                    _instance = new GlobalPaintSettings ();
                }
                return _instance;
            }
            private set {
                _instance = value;
            }
        }
    }

}