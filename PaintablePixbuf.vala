

namespace Journal {

    public class PaintablePixbuf {

        public PaintColor color { get; set; default = new PaintColor (); }

        Gdk.Pixbuf buf;

        public bool eraser_mode { get; set; default = false; }

        public int width { get; private set; }
        public int height { get; private set; }

        public PaintablePixbuf (int width, int height, bool alpha = true) {
            buf = new Gdk.Pixbuf (Gdk.Colorspace.RGB, alpha, 8, width, height);
            color = new PaintColor ();
            this.width = width;
            this.height = height;
            color.a = 255;
        }

        public void fill (PaintColor fill_color) {
            var backup_color = color;
            color = fill_color;
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    paint_pixel (x, y);
                }
            }
            color = backup_color;
        }

        public Gdk.Pixbuf get_pixbuf () {
            return buf;
        }

        public void draw_line (int x1, int y1, int x2, int y2) {
            double angle = Math.atan2 (y2 - y1, x2 - x1);

            double x = x1;
            double y = y1;


            double distance = 9999999;
            double new_distance = distance - 1;
            while (distance > new_distance) {
                draw_circle ((int) x, (int) y);
                x += Math.cos (angle);
                y += Math.sin (angle);
                distance = new_distance;
                new_distance = Math.sqrt ((x - x2) * (x - x2)
                                        + (y - y2) * (y - y2));
            }
        }

        public void draw_circle (int x, int y) {
            uint8 alpha_backup = color.a;
            paint_pixel (x, y);
            color.a = (uint8) (0.50f * color.a);
            paint_pixel (x - 1, y);
            paint_pixel (x, y + 1);
            paint_pixel (x + 1, y);
            paint_pixel (x, y - 1);
            color.a = (uint8) (0.2f * alpha_backup);
            paint_pixel (x - 1, y - 1);
            paint_pixel (x + 1, y + 1);
            paint_pixel (x + 1, y - 1);
            paint_pixel (x - 1, y + 1);
            color.a = alpha_backup;
        }

        public void paint_pixel (int x, int y) {
            if (x < 0 || y < 0)
                return;
            if (x >= buf.width || y >= buf.height)
                return;

            int pos = x * buf.n_channels + y * buf.rowstride;

            if (eraser_mode) {
                buf.get_pixels ()[pos + 0] = 0;
                buf.get_pixels ()[pos + 1] = 0;
                buf.get_pixels ()[pos + 2] = 0;
                if (buf.has_alpha) {
                    buf.get_pixels ()[pos + 3] = 0;
                }
            } else {
                buf.get_pixels ()[pos + 0] = color.r;
                buf.get_pixels ()[pos + 1] = color.g;
                buf.get_pixels ()[pos + 2] = color.b;

                if (buf.has_alpha) {
                    //detect and prevent integer-overflow
                    if (color.a > uint8.MAX - buf.get_pixels ()[pos + 3])
                        buf.get_pixels ()[pos + 3] = 255;
                    else
                        buf.get_pixels ()[pos + 3] += color.a;
                }
            }
        }
    }
}