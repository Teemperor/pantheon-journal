
namespace Journal {

    public class GridPixbuf {

        PaintablePixbuf buf;

        public int width { get; private set; }
        public int height { get; private set; }

        public GridPixbuf (int width, int height) {
            this.width = width;
            this.height = height;
            buf = new PaintablePixbuf (width, height, false);
            var black = new PaintColor (100, 100, 100, 255);
            var white = new PaintColor.from_string ("white");
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    if (x % 10 == 0 || y % 10 == 0) {
                        buf.color = black;
                    } else {
                        buf.color = white;
                    }
                    buf.paint_pixel (x, y);
                }
            }
        }

        public Gdk.Pixbuf get_pixbuf () {
            return buf.get_pixbuf ();
        }

    }
}