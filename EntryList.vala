
namespace Journal {

    public class EntryList : Gtk.ScrolledWindow {
        Gtk.Grid grid;

        int last_y = 0;

        public EntryList () {
            grid = new Gtk.Grid ();
            add (grid);
            add_events (Gdk.EventMask.BUTTON1_MOTION_MASK);
            add_page ();
            add_page ();
            add_page ();
        }

        public void scroll (double d) {
                vadjustment.value -= d;
        }

        public void add_page () {
            var entry = new EntryWidget (this);
            var left_widget = new Gtk.Grid ();
            left_widget.set_hexpand (true);
            var right_widget = new Gtk.Grid ();
            right_widget.set_hexpand (true);
            grid.attach (left_widget,   0, last_y, 1, 1);
            grid.attach (entry,         1, last_y, 1, 1);
            grid.attach (right_widget,   2, last_y, 1, 1);
            last_y++;
        }
    }
}